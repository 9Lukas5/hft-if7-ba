@startuml "implementation_classes_RegisteredSystems"

skinparam backgroundColor transparent
skinparam dpi 300

class RegisteredSystems <<(S, orange)>> {
    - {static} logger: Logger
    - {static} instance: RegisteredSystems

    # {static} getInstance(): RegisteredSystems

    .. Builder creator methods ..
}

@enduml

@startuml "implementation_classes_TicketSystem"

skinparam backgroundColor transparent
skinparam dpi 300

abstract TicketSystem {
    __ generics definition __
    T \textends Ticket
    TS \textends TicketSystem
    TB \textends TicketBuilder
    F \textends Filter
    ==
    - {static} logger: Logger
    + {static} fromUri(uri: String): TicketSystem
    + {static} fromBuilder(): RegisteredSystems
    __ instance definition __
    + apiKey: String
    + baseUrl: String
    + password: String
    + username: String
    # configuration: Map<String, String>

    + config(option: ConfigurationOptions, value: String): TS
    + config(option: ConfigurationOptions, value: boolean): TS
    + getConfigTrue(option: ConfigurationOptions): boolean
    + getConfigValue(option: ConfigurationOptions): Object

    + {abstract} createTicket(): TB
    + {abstract} find(): F
    + {abstract} getTicketById(id: String): T
    + {abstract} getTicketById(id: int): T
    
    + {abstract} hasAssigneeSupport(): boolean
    + {abstract} hasDefaultPagination(): boolean
    + {abstract} hasLabelSupport(): boolean
    + {abstract} hasPaginationSupport(): boolean
    + {abstract} hasReturnNullOnErrorSupport(): boolean
}

@enduml


@startuml "implementation_classes_TicketSystemBuilder"

skinparam backgroundColor transparent
skinparam dpi 300

abstract TicketSystemBuilder {
    __ generics definition __
    B \textends TicketSystemBuilder
    TS \textends TicketSystem
    ==
    - {static} logger: Logger
    __ instance definition __
    + String baseUrl

    + withBaseUrl(url: String): B
    + {abstract} build(): TS
}

@enduml

@startuml "implementation_classes_TicketBuilder"

skinparam backgroundColor transparent
skinparam dpi 300

abstract TicketBuilder {
    __ generics definition __
    B \t extends TicketBuilder
    T \t extends Ticket
    TS \t extends TicketSystem
    ==
    - {static} logger: Logger
    __ instance definition __
    + PARENT: TS

    # assignees: Set<String>
    # description: String
    # labels: Set<String>
    # title: String

    + assignees(identifiers: String...): B
    + description(description: String): B
    + labels(labels: Set<String>): B
    + labels(labels: String[]): B
    + title(title: String): B

    + {abstract} create(): T
}

@enduml

@startuml "implementation_classes_Ticket"

skinparam backgroundColor transparent
skinparam dpi 300

abstract Ticket {
    __ generics definition __
    TS \t extends TicketSystem
    T \t extends Ticket
    ==
    - {static} logger: Logger
    __ instance definition __
    # parent: TS

    # updatedFields: Set<String>

    # assignees: Set<TicketAssignee>
    # description: String
    # id: String
    # labels: Set<String>
    # open: boolean
    # title: String

    + getParent(): TS
    + getter(): ? \t[...]
    + setter(): T \t[...]
    + {abstract} save(): T
}

@enduml

@startuml "implementation_classes_Filter"

skinparam backgroundColor transparent
skinparam dpi 300
skinparam wrapWidth 240

abstract Filter {
    __ generics definition __
    T extends Ticket
    F extends Filter
    ==
    - {static} logger: Logger

    # {static} addToSet(filterName: FilterNames, map: Map<String, Object>, newValue: String): void
    __ instance definition __
    # setFilters: Map<String, Object>

    + withAssigneeId(id: String): F
    + withAssigneeName(name: String): F
    + withDescriptionContain(substring: String): F
    + withDescriptionMatch(regex: String): F
    + withId(id: String): F
    + isOpen(): F
    + isClosed(): F
    + withLabel(label: String): F
    + setPage(page: int): F
    + setPageSize(size: int): F
    + withTitleContain(substring: String): F
    + withTitleMatch(regex: String): F
    + {abstract} get(): List<T>
}

@enduml

@startuml "implementation_classes_TicketAssignee"

skinparam backgroundColor transparent
skinparam dpi 300

class TicketAssignee {
    + EMAIL: String
    + ID: String
    + FULLNAME: String
    + USERNAME: String

    + constructor(email: String,\nid: String, username: String,\nfullName: String)
}

@enduml

@startuml "implementation_classes_Logger"

skinparam backgroundColor transparent
skinparam dpi 300

class Logger {
    - {static} handler: Handler
    - {static} mainLogger: Logger
    - {static} logger: Logger

    + {static} getLogger(name: String): Logger
    + {static} setFormatter(formatter: Formatter): void
    + {static} setLevel(level: Level): void
    + {static} test(message: String): void
}

@enduml
