\subsubsection{Kernklassen}

\begin{figure}[h!]
    \includegraphics[width=\textwidth]{assets/pumlout/implementation_uml-overview.png}
    \caption{Aufruf Übersicht Ticketsystem Instanziierung\label{fig:class-call-overview}}
\end{figure}

Für die Nutzung der Bibliothek muss zunächst eine Instanz eines Ticketsystems erzeugt werden.
Diese stellt die Basis für alle weiteren erzeugten Objekte dar.
Für ein unterstütztes Ticketsystem müssen die abstrakten Klassen aus dem \inlinecode{core}"=Paket implementiert werden.
Außerdem muss eine Methode in der Klasse \inlinecode{RegisteredSystems} erstellt werden,
die einen Builder für dieses System erzeugt und zurückgibt.

Die Klasse \inlinecode{TicketSystem} hat statische Methoden,
um die Instanziierung eines neuen Objektes zu beginnen.
Hierfür steht die Übergabe eines \acrshort{uri} oder die Auswahl eines Builders zur Verfügung.
\bigbreak

Die \figurename{} \ref{fig:class-call-overview}
zeigt welche Klassen nacheinander aufgerufen werden,
um eine Instanz eines Ticketsystems und darauf folgend eines Tickets/einer Ticketliste zu bekommen.

Die Bibliothek folgt an vielen Stellen dem Prinzip des \Gls{fluentapi}
und ermöglicht einen Programmierstil wie bei der intensiven Nutzung von Java"=Streams.
Ein Beispiel für die Erzeugung eines Ticketsystems über den Builder"=Prozess sieht wie folgt aus:

\begin{lstlisting}[language=java, numbers=none, caption=Beispiel Instanziierung Ticketsystem durch Builder]
TicketSystem<?,?,?,?> ts = TicketSystem
    .fromBuilder()
    .gitlab()
    .withBaseUrl("www.gitlab.com")
    .withApiKey("yourApiKeyHere")
    .withProjectId(projectIdToConnectTo)
    .build();
\end{lstlisting}

Die Erstellung eines neuen Tickets oder die Suche nach existierenden Tickets wird ebenfalls
so umgesetzt.
\bigbreak

Nachfolgend werden alle Klassen des \inlinecode{core}"=Paketes aufgelistet und deren Funktion erläutert.

\newpage
\paragraph*{TicketSystem}
    \begingroup
    \begin{wrapfigure}{R}{0.6\textwidth}
        \includegraphics[width=0.6\textwidth]{assets/pumlout/implementation_classes_TicketSystem.png}
    \end{wrapfigure}
    Die Klasse Ticketsystem repräsentiert das Zielprojekt in einem externen Ticketsystem.
    Eine Instanz muss mindestens die Informationen enthalten, die den Bereich des Zielsystems,
    in dem Tickets erstellt, gesucht und bearbeitet werden sollen, eindeutig identifiziert.
    Zusätzlich kennt es die zur Ausführung benötigten Zugangsdaten, wenn diese übergeben wurden.
    
    Die Klassendeklaration ist abstrakt, da ohne systemspezifische Implementierung eine
    Instanziierung keinen Sinn ergäbe.
    \bigbreak

    Die Klasse definiert Generics für die Implementierungen von Ticket(-/Builder), Ticketsystem und Filter.
    Wird beim Deklarieren einer Ticketsystemvariable die Parametrisierung genutzt
    oder direkt die systemspezifische Implementierung als Typ verwendet,
    sind die Rückgabetypen einiger Methoden exakt festgelegt.
    Die Nutzung des systemspezifischen Typs hat den Vorteil, dass es den Code übersichtlicher wirken lässt
    und den Aufruf von zusätzlichen Methoden erlaubt, die nicht zum Kern gehören.

    Im Fall, dass verschiedene Ticketsysteme angesprochen werden sollen,
    ist es dagegen sinnvoll den generischen Typ zu verwenden,
    um sich auf die Methoden zu beschränken, die sicher von allen Systemen unterstützt werden.

    \endgroup

    \newpage
    \begin{lstlisting}[language=java, caption=Deklarationsarten eines Ticketsystems, xleftmargin=\parindent]
// Ticketsystem ohne Generics ("raw type")
TicketSystem untypedInstance;

// Ticketsystem mit unbekannten Generics
TicketSystem<?,?,?,?> anythingPossibleInstance;

// Ticketsystem mit festgelegter Implementierung
TicketSystem<ExampleTicket, ExampleTicketSystem, ExampleTicketBuilder, ExampleFilter> knownImplementationInstance;

// Ticketsystem mit direkter Nutzung der Implementierung als Typ
ExampleTicketSystem directTypedInstance;
    \end{lstlisting}
    \bigbreak

    Um eine Instanz dieser Klasse zu erzeugen, sind zwei Wege vorgesehen.
    Der erste ist ein Builder"=Mechanismus mit einem \Gls{fluentapi},
    der durch eine statische Methode der Ticketsystem"=Klasse gestartet wird.
    Gedacht ist dieser Weg für Fälle,
    in denen das System bereits klar ist und daher hart im Quellcode gewählt werden kann.

    Der zweite Weg ist die Übergabe eines \acrshort{uri},
    ebenfalls an eine statische Methode.
    Diese Möglichkeit ist für eine Anwendung vorgesehen,
    bei der der Programmcode nicht weiß, mit welchem System er arbeiten muss.
    Hier könnten die Verbindungsinformationen auch erst während der Laufzeit bekannt sein
    und unkompliziert übergeben werden.
    \bigbreak

    Für die \acrshort{uri}"=Instanziierung sollte IMMER die Methode der Kernklasse aufgerufen werden,
    nicht die gleichnamige Methode einer spezifischen Systemklasse.
    Die Kernklasse erwartet einen String der Form:\\
    \inlinecode{unifiedticketing:<system identifier>:<additional information>}

    Die Logik der Kernklasse prüft diesen String mit einem regulären Ausdruck und
    versucht das System zu identifizieren.
    Ist ein System für den Bezeichner bekannt,
    wird die statische Methode der passenden Systemklasse aufgerufen.
    Diese bekommt als Parameter allerdings nicht mehr den gesamten \acrshort{uri} übergeben,
    sondern nur noch die zusätzlichen Informationen.

    Diese Aufteilung ist darin begründet, die regulären Ausdrücke möglichst unkompliziert zu halten.

    \begin{lstlisting}[language=java, numbers=none, caption=Codevergleich Instanziierung Ticketsystem durch Builder oder \acrshort{uri}]
// Instanziierung mit Builder
TicketSystem byBuilder = TicketSystem
    // startet Fluent-Interface fuer Builder Prozess
    .fromBuilder()
    // instantiiert Builder fuer spezifische Implementierung
    .<system to instantiate>
    // setzt benoetigte Informationen die der URI ebenfalls enthalten muss
    ...<config methods>
    // erzeugt TicketSystem Instanz aus gesammelten Informationen
    .build();
// Instanziierung mit URI
TicketSystem byUri = TicketSystem.fromUri(<some variable>);
    \end{lstlisting}

\paragraph*{RegisteredSystems}
    \begin{wrapfigure}{r}{0.5\textwidth}
        \vspace{-1em}
        \includegraphics[width=0.5\textwidth]{assets/pumlout/implementation_classes_RegisteredSystems.png}
    \end{wrapfigure}
    Hierbei handelt es sich um eine Singleton"=Klasse,
    deren Instanz zum Aufruf eines Ticketsystem"=Builders genutzt wird.

    Für jedes von der Bibliothek unterstützte System gibt es in dieser Klasse eine Methode,
    die eine neue Instanz dessen System"=Builders erzeugt und zurückgibt.

\newpage
\paragraph*{TicketSystemBuilder}
    \begingroup
    \begin{wrapfigure}{l}{0.45\textwidth}
        \vspace{-1em}
        \includegraphics[width=0.45\textwidth]{assets/pumlout/implementation_classes_TicketSystemBuilder.png}
    \end{wrapfigure}
    Der Builder für ein Ticketsystem sammelt durch ein \Gls{fluentapi} alle Informationen.
    Abschließend wird mit dem Aufruf der Methode \inlinecode{build()} daraus eine neue Ticketsystem"=Instanz erzeugt.

    Die Kernklasse stellt die Methode die URL zu setzen bereit und deklariert die abstrakte Build"=Methode.
    Alle weiteren Eigenschaften, die gesetzt werden können sollen,
    müssen in der jeweiligen Systemklasse deklariert und implementiert werden.

    \endgroup

\paragraph*{TicketBuilder}
    \begingroup
    \begin{wrapfigure}{R}{0.45\textwidth}
        \includegraphics[width=0.45\textwidth]{assets/pumlout/implementation_classes_TicketBuilder.png}
    \end{wrapfigure}
    Der Ticket"=Builder wird immer von einer Ticketsystem"=Instanz aus gestartet,
    mit dem Zweck ein neues Ticket anzulegen.
    Dieser Prozess ist ebenfalls durch eine \Gls{fluentapi}"=Implementierung umgesetzt worden.

    Die Kernklasse stellt hierbei für die verschiedenen Felder direkt die Setter zur Verfügung.
    Eine Systemklasse kann hier zusätzliche hinzufügen,
    den bestehenden Settern eine Prüfung der Werte voranstellen,
    oder diese durch vollständig eigene Implementierungen ersetzen.

    Unterstützt eine Systemklasse ein Feld nicht,
    kann dies in der abschließenden Methode einfach ignoriert werden
    und mit einer Warnung über den Logger vermerkt werden,
    oder bereits beim Setter durch Ersetzen mit einer \gls{noop}"=Methode das Setzen
    dieses Feldes unterbunden werden.
    \bigbreak

    In der Implementierung der abschließenden Methode muss eine Systemklasse
    im verbundenen Ticketsystem ein neues Ticket anlegen, dass die zuvor gesammelten Informationen widerspiegelt.
    Nach erfolgtem Anlegen wird das neue Ticket als Instanz zurückgegeben.
    
    Um sicherzustellen, dass diese Instanz tatsächlich dem entspricht,
    was das externe Ticketsystem angelegt hat,
    werden für die Erzeugung dieser Ticket"=Instanz nicht die gesammelten Daten direkt verwendet.
    Stattdessen wird, wie beim Abfragen eines bestimmten Tickets,
    aus den Daten des externen Ticketsystems eine Instanz gebildet.
    Enthält die Antwort auf das Anlegen nicht bereits den gewünschten Datensatz,
    muss eine separate Abfrage des neuen Tickets erfolgen.

    \endgroup

\paragraph*{Ticket}
    \begingroup
    \begin{wrapfigure}{L}{0.45\textwidth}
        \includegraphics[width=0.45\textwidth]{assets/pumlout/implementation_classes_Ticket.png}
    \end{wrapfigure}
    Eine Instanz der Ticketklasse repräsentiert jeweils ein zugehöriges Ticket des verbundenen Ticketsystems.
    Daher ist der Konstruktor für diese Klasse geschützt.
    Eine entsprechende Ticket"=Instanz wird durch Anlegen eines neuen oder Abrufen vorhandener Tickets erlangt.

    Die Kernklasse hat Felder für die gängigsten Informationen.
    Die Getter für diese Felder liefern die Feldwerte zurück,
    die Setter immer das Ticket selbst.
    Dadurch sind Änderungen mehrerer Felder und das abschließende Speichern,
    wie bereits bei vorangegangenen Elementen,
    über ein \Gls{fluentapi} möglich.

    Die Methode, um Änderungen zu speichern, ist in der Kernklasse abstrakt,
    da die Logik hierfür wie beim Abrufen und Anlegen von Tickets systemspezifisch ist.
    \bigbreak

    Die Klasse hat eine eigene Implementierung der \inlinecode{equals}"=Methode,
    die zwei Tickets Gleichheit bescheinigt,
    wenn sie vom gleichen System sind,
    die gleiche Ticket"=Id haben und zur selben Systeminstanz gehören.

    Der Effekt dieser Implementierung verdeutlicht an der Änderung eines Tickets:
    Es wird ein Ticket abgerufen, etwas verändert und gespeichert.
    Beim Speichern wird eine \textbf{neue} Instanz der Klasse zurückgegeben,
    die dem neuen Stand im verbundenen System entspricht.
    Befindet sich die ursprüngliche Instanz in einer Menge,
    wird diese durch die neue ersetzt,
    wenn diese derselben Menge hinzugefügt wird.
    Ohne die eigene Gleichheitsprüfung enthielte die Menge beide Instanzen.
    \bigbreak

    Das Feld \inlinecode{updatedFields} ist eine Menge von String"=Literalen der Felder,
    deren Werte geändert wurden.
    Die Kernklasse enthält eine Enumeration als innere Klasse.
    Es werden allerdings die String"=Werte statt direkt der Enumeration verwendet,
    um den Systemklassen die Möglichkeit zu geben,
    spezifische Felder zu führen und deren Änderungen im selben Feld der Kernklasse zu markieren.

    Wird die Speichern"=Methode aufgerufen und es sind keine Felder als geändert markiert,
    sollte eine Systemimplementierung idealerweise einfach sich selbst zurückgeben,
    statt eine Anfrage an das externe Ticketsystem zu senden.

    \endgroup

\paragraph*{Filter}
    \begingroup
    \begin{wrapfigure}[14]{r}{0.5\textwidth}
        \vspace{-3em}
        \includegraphics[width=0.5\textwidth]{assets/pumlout/implementation_classes_Filter.png}
    \end{wrapfigure}
    Bei der Filterklasse handelt es sich in gewisser Weise erneut um einen Builder.
    Durch den Aufruf der Methode \inlinecode{filter} eines Ticketsystems
    wird ein für dieses System passendes Objekt der Filterklasse instantiiert.

    Diese bereitet den Abruf einer Liste von Tickets aus dem verbundenen Ticketsystem vor.
    Die Klasse bietet erneut eine \Gls{fluentapi}"=Implementierung,
    über die Filter festgelegt werden können, denen die Tickets entsprechen sollen.

    Manche Filter überschreiben den vorherigen Wert desselben Typs bei mehrfachem Aufruf,
    andere werden um den neuen Wert erweitert.
    So gilt bei den Filtern für Beschreibung und Titel, Offen"=/Geschlossen"=Zustand, Seitenzahl sowie Größe
    immer der Wert des letzten Aufrufs.
    Bei IDs und Namen zugeteilter Personen, Ticket"=Ids sowie den Labels wird eine Liste geführt,
    die mit jedem Aufruf erweitert wird.
    Die in Listen geführten Filter werden in der Regel als und"=Verknüpfung gewertet.
    Einzige Ausnahme sind die Ticket"=IDs, diese werden oder"=verknüpft.
    Bei einem Filter nach drei IDs und drei Labeln,
    enthält die Ergebnismenge nur die Tickets,
    deren ID teil der angegebenen IDs ist und die alle angegebenen Label haben.

    Manche Filter, die die Kernklasse anbietet,
    werden von dem verbundenen Ticketsystem eventuell nicht unterstützt.
    Eine Implementierung kann einen solchen Filter dann beispielsweise mit einer Warnung ignorieren,
    und/oder diesen im Nachgang der Abfrage selbst umsetzen.

    Ein Beispiel dafür dürfte der Match"=Filter für Titel und Beschreibung sein.
    Dass ein System einen Filter nach einem regulären Ausdruck anbietet,
    dürfte eher unwahrscheinlich sein.
    \bigbreak

    Um \textquote{alle} Tickets aus der Verbindung abzurufen,
    wird die Abschlussmethode einfach ohne vorherige Filter zu setzen aufgerufen.
    Die Aussage \textquote{alle Tickets} ist dabei dennoch meist mit Vorsicht zu genießen:
    die meisten Ticketsysteme paginieren standardmäßig.
    Die Tickets, die ohne Filter zurückkommen, müssen also nicht zwingend alle existierenden sein.

    Um zuverlässig alle Tickets zu bekommen,
    muss eine Seitengröße gesetzt werden und in einer Schleife mit aufsteigender Seitenzahl die Aufrufe wiederholt werden,
    bis das Ergebnis des einzelnen Aufrufs weniger Tickets enthält, als die gesetzte Seitengröße.

    \endgroup

\paragraph*{TicketAssignee}
    Die Klasse TicketAssignee ist eine der wenigen nicht"=abstrakten Klassen im Kernpaket der Bibliothek.
    Sie ist als Repräsentation der verantwortlichen Person in einer Ticket"=Instanz gedacht.

    \begingroup
    \begin{wrapfigure}{l}{0.4\textwidth}
        \includegraphics[width=0.4\textwidth]{assets/pumlout/implementation_classes_TicketAssignee.png}
    \end{wrapfigure}
    Die Felder, die zur Verfügung gestellt werden, sind alles String"=Felder für die Interoperabilität.
    Da die Bibliothek keine Veränderungen an Nutzerinformationen vornehmen soll,
    sind die Felder allesamt konstant und müssen mit dem Konstruktoraufruf befüllt werden.
    Hat ein System für ein Feld keine Informationen,
    wird es einfach mit \inlinecode{null} befüllt.

    Die Klasse hat zum Vergleich mehrerer Verantwortlicher eine eigene Implementierung,
    die das Klassenfeld \inlinecode{id} zum Vergleich heranzieht.

    \endgroup

\paragraph*{Logging}
    \begingroup
    \begin{wrapfigure}{R}{0.5\textwidth}
        \vspace{-1em}
        \includegraphics[width=0.5\textwidth]{assets/pumlout/implementation_classes_Logger.png}
    \end{wrapfigure}
    Die ganze Bibliothek erhielt umfassende Logging"=Aufrufe.
    Der größte Teil davon nutzt die Stufen \textquote{FINE}, \textquote{FINER} und \textquote{FINEST}.
    Eingesetzt wurde dafür die von Java mitgelieferte Lösung aus dem Paket \inlinecode{java.util.logging}.

    Die eigene Klasse \inlinecode{Logger} stellt eine statische Methode
    \inlinecode{getLogger(String name)} zur Verfügung,
    über die die Klassen der Bibliothek ihre Logger"=Instanz beziehen.
    Dieser scheinbare Umweg stellt sicher, dass der statische Konstruktor der Klasse garantiert aufgerufen wird.
    In diesem wird der Logger für das Hauptpaket der Bibliothek aufgerufen,
    die Nutzung des übergeordneten Handlers abgeschaltet
    und ein eigener \inlinecode{ConsoleHandler} mit einer einzeiligen Formatierung gesetzt.

    Das standardmäßig gesetzte Log"=Level ist \textquote{INFO}.
    Über die statische Methode \inlinecode{setLevel} kann ein eigenes Level gesetzt werden,
    über \inlinecode{setFormatter} ein eigenes Ausgabeformat.

    Die Methode \inlinecode{test(String message)} loggt die übergebene Nachricht in jedem möglichen Level.
    \bigbreak

    Um in einem Projekt den übergeordneten Logger und dessen Format zu verwenden,
    muss nach der statischen Initialisierung der Logger"=Klasse der Bibliothek der Logger vom Hauptpaket der Bibliothek neu konfiguriert werden.

    \endgroup
