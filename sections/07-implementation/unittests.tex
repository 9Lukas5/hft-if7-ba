\subsection{Unittests}\label{chapter:impl:tests}
Zur Funktionsprüfung und Qualitätssicherung wurden für den Kernteil sowie die Systemimplementierung Unittests geschrieben.

Die Abdeckung der Tests soll die korrekte Funktion der verschiedenen Methoden überprüfen.
Getter werden beispielsweise durch direktes Beschreiben des Feldes
und anschließendem Getter"=Aufruf getestet.
Bei den Settern dementsprechend anders herum,
durch Beschreiben über den Setter und Prüfung des Feldes durch direkten Zugriff.

Bei Methoden die eine \textquote{Berechnung} durchführen,
wird entsprechend eine erwartete Rückgabe vorbereitet und dann gegengeprüft.

Zusätzlich zur Sicherstellung,
dass bei korrekten Eingaben alles wie erwartet funktioniert,
werden die Methoden auch gezielt mit fehlerhaften Werten/Vorbedingungen aufgerufen.
Ziel dieser Aufrufe ist sicherzustellen,
dass falsche Eingaben nicht ungeprüft angenommen werden und später eventuell zu unvorhergesehenen Auswirkungen führen.
Stattdessen wird hier ebenfalls ein vordefiniertes Verhalten erwartet,
dass getestet wird.
\bigbreak

Als Test"=Framework kommen die JUnit Jupiter"=Engine von JUnit5 \cite{junit_what-is-junit5}
sowie Mockito \cite{mockito_intro} zum Einsatz.

Mockito ist für die Unittests dieser Bibliothek an \textbf{zwei Stellen} ein wichtiges Schlüsselelement,
die nachfolgend erläutert werden.

\subsubsection{Testen abstrakter Klassen}
    Die Klassen des Kernpaketes sollten unabhängig von einer Implementierung testbar sein.
    Da die meisten dieser Klassen allerdings abstrakt sind,
    kann von ihnen in der Regel nicht ohne Implementierung eine Instanz gebildet werden.

    Eine mögliche Lösung,
    um ohne zusätzliche Abhängigkeit auszukommen,
    wäre eine Dummy"=Implementierung als innere Klasse in der Testklasse.
    Das hat allerdings den gravierenden Nachteil von sehr viel \textquote{totem} Quellcode,
    da für alle abstrakten Klassen eine \textquote{Implementierung} nötig ist,
    obwohl nur der nicht"=abstrakte Code der Klassen getestet wird.

    Mockito ermöglicht hier das instantiieren eines \textquote{Mocks}.
    Standardmäßig hat dieses so erzeugte Scheinobjekt die Methoden,
    wie eine echte Implementierung sie hätte.
    Werden diese Aufgerufen,
    passiert allerdings nichts.
    Um die echte Logik aufzurufen,
    muss die Mock"=Instantiierung etwas justiert werden.
    \bigbreak

    Das Beispiel im \lstlistingname{} \ref{lst:mock-creation} zeigt die Erzeugung eines Scheinobjektes,
    dass dennoch die tatsächliche Logik aufruft, anstatt einer \Gls{noop}:

    In der Zeile 4 beginnt die Mock"=Erzeugung, in diesem Fall mit zwei Parametern.
    Der Erste Parameter, in Zeile 5, muss die Klasse sein, die instantiiert werden soll.
    In den Zeilen 6 bis 8 wird ein Konfigurationsobjekt für die Instantiierung definiert.

    Diese Konfiguration legt einmal in der Zeile 7 die Nutzung eines bestimmten Konstruktors der Klasse,
    sowie in der Zeile 8 das Aufrufen der tatsächlichen Logik anstatt einer \Gls{noop} fest.

    Der in Zeile 7 festgelegte Konstruktor bekommt ein weiteres Scheinobjekt der Ticketsystem"=Klasse übergeben.
    Bei diesem wird allerdings nichts konfiguriert,
    da für die vorgesehenen Tests keine Interaktion der Ticketklasse mit der zugehörigen Systeminstanz passieren soll.


    \begin{lstlisting}[language=java, caption=Beispiel-Instantiierung eines Mock-Objektes das tatsächliche Logik aufruft, label={lst:mock-creation}, xleftmargin=\parindent]
@BeforeEach
public void initBeforeEach()
{
    instance = mock(
        Ticket.class,
        withSettings()
            .useConstructor(mock(TicketSystem.class))
            .defaultAnswer(CALLS_REAL_METHODS)
    );
}
    \end{lstlisting}

\subsubsection{Testen ohne reales Backend}
    Das Testen der Logik der einzelnen Ticketsystem"=Integrationen
    muss ohne Kommunikation mit einer richtigen Gegenstelle stattfinden.
    Daher müssen die Webanfragen, die im Produktiveinsatz gestellt werden,
    beim Testen abgefangen und durch falsche Rückgabewerte ersetzt werden.

    Für die gezeigte Systemimplementierung wurde die \textquote{OkHttp3 \cite{SquareInc_OkHttp3-docs-home}} Bibliothek verwendet.
    Für eine Webanfrage wird eine Instanz der Klasse \inlinecode{OkHttpClient} benötigt,
    der eine Instanz der Klasse \inlinecode{Request} übergeben wird
    und eine Instanz der Klasse \inlinecode{Response} zurückgibt.

    Der einfachste Punkt in dieser Kette anzusetzen,
    ist die Instantiierung des \inlinecode{OkHttpClient}"=Objektes.
    Dieses Objekt wird mit einem Standardkonstruktor erzeugt
    und die gesamte Webanfragen"=Kette kann von diesem ausgehend manipuliert werden.
    \bigbreak

    Mockito bietet neben den Scheinobjekten (Mock),
    auch Späher"=Objekte (Spy) an.
    Dabei wird ein Objekt ganz normal instantiiert und anschließenden
    mit einem Späher versehen.
    Dieser bietet dann zum einen passive Möglichkeiten,
    wie das Zählen wie oft, oder mit welchen Parametern, eine Methode aufgerufen wurde,
    zum anderen aber auch aktive Eingriffsmöglichkeiten,
    wie eine andere Logik/einen anderen Rückgabewert bei Aufruf einer Methode.

    Die Möglichkeit eine Logik vollständig zu ersetzen wird für das Unterbinden tatsächlicher Webanfragen eingesetzt.
    Um dies einfach zu gestalten,
    wurde die Instantiierung des \inlinecode{OkHttpClient} in eine separate Methode der jeweiligen Klasse ausgelagert.

    \noindent
    \begin{minipage}[h]{0.39\linewidth}
        \begin{lstlisting}[language=java, title=normale Instantiierung, numbers=none]
public Object someMethod()
{
    OkHttpClient client =
        new OkHttpClient();
}
        \end{lstlisting}
    \end{minipage}
    \begin{minipage}[h]{0.6\linewidth}
        \begin{lstlisting}[language=java, caption=Ausgelagerte Instantiierung für Mockito Zugriff, label={lst:interceptionable-httpclient-init}, numbers=none]
protected OkHttpClient getHttpClient()
{
    return new OkHttpClient();
}

public Object someMethod()
{
    OkHttpClient client = getHttpClient();
}
        \end{lstlisting}
    \end{minipage}
    \bigbreak

    Im Unittest wird diese Methode dann einfach ersetzt und ein Dummy zurückgegeben,
    der das für den Test benötigte Ergebnis liefert.
    Die Initialisierung dafür, die vor jeder Testmethode ausgeführt wird,
    sieht für die Testklasse des \inlinecode{GitlabTicket} beispielsweise folgendermaßen aus:

    \begin{lstlisting}[language=java, caption=Unittest Initialisierung für simulierte Webanfragen, xleftmargin=\parindent]
@BeforeEach
public void initBeforeEach()
{
    instance = spy(new GitlabTicket(mock(GitlabTicketSystem.class)));
    instance.getParent().baseUrl = "https://example.org";

    OkHttpClient client = mock(OkHttpClient.class);
    call = mock(Call.class);
    requestCaptor = ArgumentCaptor.forClass(Request.class);

    doReturn(client).when(instance).getHttpClient();
    doReturn(call).when(client).newCall(requestCaptor.capture());
}
    \end{lstlisting}

    In Zeile 4 wird die zu testende Instanz erzeugt und mit einem Späher versehen.
    Da ein Ticket eine Ticketsystem"=Instanz als Parameter benötigt,
    wird hier ein Scheinobjekt an den Konstruktor übergeben.

    Im Laufe der Erstellung der Webanfrage nimmt das Ticket die URL vom Ticketsystem,
    zu dem es gehört.
    Da nur ein Mock übergeben wurde, gäbe diese \inlinecode{NULL} zurück.
    Daher wird in Zeile 5 dem entsprechenden Feld des Ticketsystem"=Mocks ein valider Wert gesetzt.

    In den Zeilen 7 und 8 werden Mocks für den \inlinecode{OkHttpClient}
    und den daraus erzeugten \inlinecode{Call} erstellt.
    Zeile 9 instantiiert ein Objekt um übergebene Parameter des Typs \inlinecode{Request} aufzuzeichnen.

    Die Zeile 11 bewirkt letztlich,
    dass beim Aufruf von \inlinecode{getHttpClient()} nicht wie im \lstlistingname{} \ref{lst:interceptionable-httpclient-init} gezeigt,
    eine neue Instanz erzeugt,
    sondern der Mock aus Zeile 7 zurückgegeben wird.

    Da es sich nur um ein Mock"=Objekt handelt,
    muss jetzt für jede Methode,
    von der erwartet wird,
    dass sie aufgerufen wird,
    eine entsprechende Handlung definiert werden.
    Daher definiert Zeile 12 die Rückgabe des \inlinecode{Call}"=Mocks aus Zeile 8.

    Der Methode \inlinecode{newCall} muss ein Objekt der Klasse \inlinecode{Request} übergeben werden.
    Durch den hier zusätzlich gesetzten Rekorder ist das übergebene Objekt im Test auswertbar.
    \bigbreak

    Durch die Zeilen 11 und 12 wird noch nicht die gesamte Kette der Webanfrage abgedeckt,
    aber Sie stellen die Basis dar,
    die für alle Tests identisch ist,
    weshalb sie in der globalen Initialisierung stehen.

    Der Rest der Kette wird in den einzelnen Testmethoden definiert,
    um verschiedene Szenarien abzudecken.
    Hierzu gehören erfolgreiche Anfragen, bei denen eine vordefinierte Antwort zurückgegeben wird,
    sowie verschiedene Fehler,
    bei denen eine bestimmte Reaktion erwartet wird.
