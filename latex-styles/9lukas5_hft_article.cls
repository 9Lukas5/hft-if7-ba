\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{9lukas5_hft_article}[2020/12/05 9Lukas5 hft article class]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions
\LoadClass{article}

\RequirePackage{fontspec}
\RequirePackage[babelshorthands=true]{polyglossia}
\RequirePackage{textcomp}
\RequirePackage[utf8]{inputenc}
\RequirePackage{csquotes}

\RequirePackage{fancyhdr}

\RequirePackage{babel}

\RequirePackage{geometry}

\RequirePackage{caption}
\RequirePackage{xcolor}
\RequirePackage{titlesec}

\RequirePackage{setspace}

\setstretch{1.5}

\setdefaultlanguage{german}

\setmainfont{DejaVu Serif}
\setsansfont{DejaVu Sans}
\setmonofont{DejaVu Sans Mono}

\newcommand{\pageOf}{of}
\addto\captionsgerman{
    \renewcommand{\pageOf}{von}
}

\makeatletter
\def\theAuthor{\@author}
\def\theTitle{\@title}
\makeatother

\newcommand{\makePdfMetadata}{
    \hypersetup{
        pdfauthor={\theAuthor},
        pdftitle={\theTitle}
    }
}

\captionsetup{font={footnotesize,sf}}

\geometry
{
    a4paper,
    top=20mm,
    bottom=20mm,
    left=35mm,
    right=20mm,
    includehead,includefoot,
    headheight=27pt
}

% headings
\definecolor{sectionHeading}{RGB}{60,60,200}
\definecolor{HFTRED}{RGB}{200,60,60}

% table row colors
\definecolor{tableHeaderRow}{RGB}{110,110,200}
\definecolor{tableOddRow}{RGB}{230,230,255}
\definecolor{tableEvenRow}{RGB}{250,250,255}

\titleformat
{\section} % command
%[] % shape
{\color{sectionHeading}\normalfont\bfseries\LARGE} % format
{\thesection} %label
{1em} % separation
{} % before code
[\titlerule] % after code

\titleformat
{\subsection} % command
%[] % shape
{\color{sectionHeading}\normalfont\bfseries\Large} % format
{\thesubsection} %label
{1em} % separation
{} % before code
%[] % after code

\titleformat
{\subsubsection} % command
%[] % shape
{\color{sectionHeading}\normalfont\bfseries\large} % format
{\thesubsubsection} %label
{1em} % separation
{} % before code
%[] % after code

\titleformat
{\paragraph} % command
%[] % shape
{\color{sectionHeading}\normalfont\bfseries} % format
{\theparagraph} %label
{1em} % separation
{} % before code
%[] % after code

\titleformat
{\subparagraph} % command
%[] % shape
{\color{sectionHeading}\normalfont\itshape} % format
{\theparagraph} %label
{1em} % separation
{} % before code
%[] % after code

\titlespacing*{\paragraph}
{0pt}
{10pt}
{1.5pt}

\titlespacing*{\subparagraph}
{0pt}
{10pt}
{1.5pt}

% footer
\newcommand{\footerText}{}
\newcommand{\licenseText}{}
\newcommand{\versionText}{}
\newcommand{\setFooterText}[1]{\renewcommand{\footerText}{#1\\}}
\newcommand{\setVersionText}[1]{\renewcommand{\versionText}{version: #1}}
\newcommand{\setLicenseText}[1]{\renewcommand{\licenseText}{ | license: #1}}

\newcommand{\frontmatter}[1]{
    \setMatter{Roman}{#1}
}

\newcommand{\mainmatter}[1]{
    \setMatter{arabic}{#1}
}

%front/main matter
\newcommand{\setMatter}[2]{
    \cleardoublepage
    \pagenumbering{#1}
    \fancypagestyle{parametrizedMatter}{
        \fancyhf{}

        \fancyhead[L]{\sffamily\color{HFTRED}Hochschule \textbf{\textit{für Technik}\\Stuttgart}}
        \fancyhead[C]{\sffamily\small\theTitle}
        \fancyhead[R]{\sffamily\small\today}
        \renewcommand{\headrulewidth}{0.4pt}

        \ifx&\footerText&
            {}
        \else
            \fancyfoot[L]{\sffamily\small\footerText
            \footnotesize\textcolor{gray}{\versionText\licenseText}}
        \fi
        \fancyfoot[R]{\sffamily\small \pagename{} \thepage{} \pageOf{} #2}
        \renewcommand{\footrulewidth}{0.4pt}
    }
    \pagestyle{parametrizedMatter}
}
