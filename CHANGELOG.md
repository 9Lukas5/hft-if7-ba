# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.0.0](https://gitlab.com/9Lukas5/hft-if7-ba/-/compare/0.9.0...1.0.0) (2021-01-07)


### Redactional Changes

* **concept:** incorporate suggestion from prof ([91b8bdc](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/91b8bdcddc3a8c1ab5a53461a8329af91e316b2c))
* **concept:** rephrase concept chapter introduction ([48471a4](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/48471a461bc7222fdf302498f0f9b3a3dd2ef66e))
* **summary:** add another response ([1da4228](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/1da42289660c9659c342026aa526466f50af00d3))
* **titlepage:** change subtitle of thesis ([c1015ce](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/c1015ce60170943d362d9dd603c113aa9b689a01))

## [0.9.0](https://gitlab.com/9Lukas5/hft-if7-ba/-/compare/0.8.0...0.9.0) (2021-01-03)


### Redactional Changes

* incorporate grammar, wording & styling fixes ([d40f737](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/d40f737e278cd05e175a35460f90ef939fb5c1ef))
* **evaluation:** rephrase 'time ends up in smoke' ([ee46b9d](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/ee46b9dbb7ac9fd5c88e43f90bf27c47f0229912))
* **fundamentals:** rephrase summary ([2003e3c](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/2003e3ca92a5c2242e460c98c118be6f036b1784))
* **glossary:** remove citations from acronyms ([194c610](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/194c610ff44e1eee5721b01947f91d39ef871188))
* **impl/core:** fix and expand explanation for list behaviour in filters ([c7e496f](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/c7e496fd4e7e2309b09e70d19af5d6d6bde860e9))
* **impl/systems:** add broader explanation for ext http lib usage ([28dc368](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/28dc368a52a9331a5b579525794da4515a7d5612))
* **look-ahead:** rephrase/simplify markup language getter description ([beab5a0](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/beab5a02e5e921d0558d6e08255e10c1bd602f19))
* **main:** remove appearance list from glossaries and acronyms ([ca78c11](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/ca78c11119f3511cceb345565a7a524aca695f6f))
* **modocot:** rephrase why static artifact name is required ([b71f1a3](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/b71f1a3f50ca3171ec9b2aa9a13844f8813afa81))
* **research:** rephrase chapter introduction ([437072b](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/437072bbab30d8bf54cd70bd7826ccc954b7fbdf))

## [0.8.0](https://gitlab.com/9Lukas5/hft-if7-ba/-/compare/0.7.0...0.8.0) (2020-12-25)


### Bug Fixes

* add phantomsection before bib for correct toc page number ([fdfa8d7](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/fdfa8d7590682a4fff01c21823b4ac68eb882b6e))


### Content Changes

* **assets:** add screenshots for ticket response examples ([e1bcb75](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/e1bcb758170df37be20197825cab1f25b658f677))
* **assets/plantuml:** add modocot old&new sequence diagram ([f21e83c](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/f21e83cec6fc3c316e12ec27896fadaa7594dcec))
* **concept:** write concept for Modocot ([d40c501](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/d40c50138f9e117c9cfe5a2ab475bb7cd60cbc69))
* **evaluation:** add final evaluation chapter ([247eced](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/247eced78ed28d4c9a48b477a1db7725cc9f3505)), closes [#8](https://gitlab.com/9Lukas5/hft-if7-ba/-/issues/8)
* **look-ahead:** add final chapter with future directives ([d7af40f](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/d7af40ff9f3245d2a16dc09b96606cbd08913cca)), closes [#9](https://gitlab.com/9Lukas5/hft-if7-ba/-/issues/9)
* **modocot:** add bottom line subsection to refactor section ([e0ad19a](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/e0ad19aa02f6a4330d6524cfaa77f512d8101f76))
* **modocot:** write whole modocot chapter ([9913977](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/99139774455a301bf81cada3bec5c4ef92cbaa96))
* **summary:** rewrite most part of summary, matching the final state ([17145d2](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/17145d2fccfab2fa4d53407c1317507896eb6880))


### Style Changes

* **concept:** remove some breaks ([3fe8f02](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/3fe8f02a9eb4a8da5866774e2019678ee443db04))
* **concept:** set fixed line count for wrapfig, remove another break ([ef5bb69](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/ef5bb697cddf25696e9cc4398c84484e32ccd15b))
* **fundamentals:** tweak breaks placement ([2e374c1](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/2e374c120ad81b72682a56630ae855a5c6db7def))
* **modocot:** fix image placement after adding bottom line section above ([74a0aa5](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/74a0aa521872f7e65cb6d73d83a76769bd1e8492))
* **research:** tweak breaks placement ([5ecb64e](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/5ecb64ec21b995b0426ded0b36df13b4cfad8a3a))
* **research:** tweak breaks placement ([55ebf83](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/55ebf83e233be1a88f50319f659c33918d3db12b))
* **research/bugzilla:** tweak breaks placement ([87895b2](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/87895b28dfca3c397300f7ba890c0ddecf2618f4))
* **research/evaluation:** tweak breaks placement ([8723059](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/8723059b2c0c2512d267827dd3325837b1916bf9))
* **research/fogbugz:** tweak breaks placement ([f332073](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/f332073a8b2c2aa82d6352c4f1025484c9157a33))
* **research/github:** tweak breaks placement ([aff74b2](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/aff74b28db29db48373102a7a55bebac9fefce60))
* **research/gitlab:** tweak breaks placement ([9e8675f](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/9e8675fff62f0c0169f783bac065f5b7e93e4b77))
* **research/jira:** tweak breaks placement ([30526cc](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/30526cc9074f23169c0d722fda7ccf79eb3fcb7a))
* **research/mantisbt:** tweak breaks placement ([4cf1f86](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/4cf1f86c7d91252e12e86a1237113993e1525ce0))
* **research/openproject:** tweak breaks placement ([0519174](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/0519174bd62fbd3c9d078b18df54f050f7d28735))
* **research/redmine:** tweak breaks placement ([9546865](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/95468650c596fea9b1abf9e50bbdefa0506bbf13))
* **research/summary:** tweak breaks placement ([4338652](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/4338652c30e9c5335d33952a863a1f7c0d3af2d9))
* **research/trac:** tweak breaks placement ([a2ed153](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/a2ed153976b5219f30658e63dae1c67a6a0136cd))


### Redactional Changes

* **concept:** add spaces around slash ([f05a956](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/f05a9566c3b1e43cf74514949b0bb382c8b7ad60))
* **concept:** add TODO for modocot part ([2669bd9](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/2669bd959793ae5b7f080f205071896d752a4eac))
* **glossary:** add JDK/JRE ([4f437da](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/4f437dacbe8fe9f715f0ca7cb93be90d81e73d7a))
* **glossary:** add modocot ([be81447](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/be81447e231064e02178a80985898569088b17d0))
* **impl:** extend title of chapter ([5227fe4](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/5227fe4fa3420e11dcfc6c2a31a6360c0caf8e46)), closes [#12](https://gitlab.com/9Lukas5/hft-if7-ba/-/issues/12)
* **impl/build:** update listing with final code snippets ([0763a6a](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/0763a6afe96cae6146bd95d3e7af1dc57f59da34))
* **modocot:** grammar fixes ([984aaea](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/984aaea672ad372fd5d22e5a5144cb17da991be9))
* **modocot:** grammar fixes ([a008481](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/a008481d910c94a05b0076c09a25beba35e1a180))
* **modocot:** relabel bottom line subsectin to 'Evaluation' ([3774dee](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/3774dee1c962afd4df275d4dc726b1879638e11d)), closes [#13](https://gitlab.com/9Lukas5/hft-if7-ba/-/issues/13)
* **modocot:** remove to breaks ([a5b0221](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/a5b0221760b33a8431fa9f557aee49dd673d6063))
* **modocot:** reword refactoring from 'Umbau' to 'Kernsanierung' ([e09796f](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/e09796f4bb2cf04417ee3866d946b74ff246fb94))
* **summary:** remove a brake, swap words for wording reasons ([018a16a](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/018a16ad7eadff3e7c8c19a529fcaddfce097021))
* **tasks:** update tasks to final state ([7531f37](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/7531f373f72ad812c4e0a1688fc0583685ef4b4a))

## [0.7.0](https://gitlab.com/9Lukas5/hft-if7-ba/-/compare/0.6.0...0.7.0) (2020-12-10)


### Bug Fixes

* **implementation/core:** fix listings pos inconsistency ([c50549d](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/c50549df21902ca2c413565ab0265564880a7ffd))


### Style Changes

* set listings caption pos globally to bottom ([37a7679](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/37a76795c28a5a7f4b244455e2f3e39ea99b51c1))
* **fundamentals:** move plantuml example class up a bit ([62a2223](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/62a2223ce9c3566215ae9f3a83872e88a61a1d1c))
* **implementation/build-process:** update pagebreaks/bigbreaks after linespacing change ([67fea43](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/67fea43bb48faf51e9d72dee94e4489302548bdf))
* **implementation/core:** update pagebreaks/bigbreaks after linespacing change ([60e4731](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/60e4731a860bc8a611d48abfa337914f6cb7c2b5))
* set real 1.5 line spacing ([706ca4a](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/706ca4a41f7564acd63133adafa4a3e2f40bf66b))


### Redactional Changes

* make use of new acronyms document wide 2 ([480f1b0](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/480f1b09da2296c08ce8fe425dc8f09bf3069e4d))
* **fundamentals:** add chapter introduction ([9bd1d3f](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/9bd1d3fc85e44392f272372fa73be6ad5a8fce56))
* **fundamentals:** add chapter introduction ([f8d5ba1](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/f8d5ba1a707782bef03319848f0608f017a201a2))
* **fundamentals:** remove misleading part-sentence ([1d38704](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/1d38704a0fa52359c88986ca0b47844bfaf1ff06))
* **fundamentals/plantuml:** add missing word ([ce7599a](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/ce7599a4bb310ea53c9662372c2a8e392a048187))
* **fundamentals/plantuml:** fix dass <-> das confusion ([8cb0388](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/8cb03888a57184cf76fb87371bf6ceb926f64a9a))
* **glossary:** add entries for GNU, GPL and BSD with bib entry ([4ab04a5](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/4ab04a5750b2804cf420e78c904bf4c911c56034))
* **glossary:** add many new entries, some with bib entry ([1fa0dd7](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/1fa0dd76fe50e80eb17b5cc219ed0c87c106032d))
* make use of new acronyms document wide ([81da5f2](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/81da5f2741ccb44fab11d043dca6df013f7acf27))
* **fundamentals:** remove misleading part-sentence ([3378b24](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/3378b2483de02b22ffa64d543253b471b9051373))
* **fundamentals/plantuml:** add image capture ([4ebceb1](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/4ebceb1c475d43c8e6bebbdd58f950d6ba6c3afc))
* **fundamentals/plantuml:** add missing word ([4ca8ce7](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/4ca8ce7fd066cab6c060b04e39eba12cda5cf7c4))
* **fundamentals/plantuml:** fix dass <-> das confusion ([536516c](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/536516c8ee41d11c26581b3a40d5435030abe17a))
* **glossary:** add entries for GNU, GPL and BSD with bib entry ([f823028](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/f8230282d36309d48c64f7a802360dd7557b0be4))
* **glossary:** add many new entries, some with bib entry ([4ab96e3](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/4ab96e32173de35f28b6c82a1986b9dddeb6e67a))
* **glossary:** fix typo in ci/cd ([c7c35d5](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/c7c35d5e209ca792bff5d869698c572ac7d9ab03))
* **impl/build:** more consistent usage of inlinecode ([079974a](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/079974aa14f22e3c761283fa91dcdc1e056d0da2))
* **impl/core:** remove image caption from text; it's under the image yet ([c744dcd](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/c744dcdcfd8433837d8b0debc8620193c04ed3d5))
* **impl/docs:** reword some colloquial phrases ([f73160f](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/f73160f8c5fabab9b945aa7453437dbffd4f3d77))
* **impl/systems:** add missing caption ([16f4a46](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/16f4a46f1a6765d81139fe6f260c685068301c5b))
* **impl/systems:** add missing hyphen ([e32e5f4](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/e32e5f4726a01ec185da73e1fdb38588383d6c22))
* **impl/systems:** remove false comma ([a590d86](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/a590d8618dfc62e4952f079a8c566ae141d7bd90))
* **impl/tests:** fix typo in caption ([0c0b380](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/0c0b3807fa07eb9ce09457f2fcabdefee49d1373))
* **impl/tests:** remove to comma's ([c322f77](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/c322f7720c4d79ec45903fd37cdc0b9c07b62266))
* **impl/tests:** use single word Produktiveinsatz instead of hyphen split ([1a4f54e](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/1a4f54e8a3c7335965550ef920cf3f595fc5de21))
* **introduction:** rephrase first sentence to be less colloquial ([9eaa375](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/9eaa3758533ac97a12a592c82d49c35c24f4ba8e))
* **research:** fix typo ([af7074b](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/af7074b3eadc930cf038601e8b9536cca8cd04ae))
* **research:** fix typo ([4892f0b](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/4892f0bb0d85449ff64b5f4dce2dd7baa459a582))
* **research/evaluation:** fix typo ([1a3357a](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/1a3357a99daa4608246e96d8bc655b123bfd5f9d))
* **research/fogbugz:** make less colloquial ([c3dad45](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/c3dad45a5d80866705cd7fcabea65b85bfc62186))
* **research/jira:** make less colloquial ([5dd1032](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/5dd1032a965c6cee003e4ff7e007af50538575fe))
* **research/jira:** make less colloquial ([d0db715](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/d0db7157b351745405c17f3cc0828556c62aa184))
* **research/trac:** make use of GNU, GPL and BSD acronyms ([17db747](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/17db74776b2c2ed7cb8527f2c292cbeba048aa7b))
* **research/trac:** make use of GNU, GPL and BSD acronyms ([8cd6681](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/8cd6681bab3c52dfa488af08f21073dee99ec152))
* **tasks:** add chapter introduction ([95bc371](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/95bc371a65a4182b2d182238407546eea7de49ca))
* **tasks:** add chapter introduction ([6daa9e5](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/6daa9e5ff4bed671f22f541d43ea05a376ffe0d4))
* **tasks:** fix typos ([6b90a06](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/6b90a062da337dca734e510187f7671e7457ac1b))
* make consistent use of inlinecode for class and method names ([9f7190f](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/9f7190f38cead1b0168d3290504a9064f1e3f63d))
* **tasks:** fix typos ([bbc0eae](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/bbc0eae82391766d28a35e400ef5381e60b3a161))
* renew command for listings index name ([77ea6e3](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/77ea6e359562bd003890bc5fb721088375a5b7c1))


### Content Changes

* **ticket-research:** add poll results ([a277205](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/a27720526d7bb9d718694c924d645861b9decc7e))
* **ticket-research:** show poll results & update conclusion ([c4c4277](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/c4c4277c7e18fad4f3b70cac2fbc58089294df4e))

## [0.6.0](https://gitlab.com/9Lukas5/hft-if7-ba/-/compare/0.5.0...0.6.0) (2020-12-04)


### Content Changes

* **implementation:** add assets for docs chapter ([6a778d2](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/6a778d2535fc4fe44a5d276cbd24b84e6d71fdcc))
* **implementation:** write documentation chapter ([acb887c](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/acb887c3a6c0e4fcd8cd5df0d7d38cb65cfe3922))


### Style Changes

* **implementation:** move build-process to new page ([fdf9ef4](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/fdf9ef493191e7750a769e2b77261a714a713eb8))
* **implementation/build-process:** rearrange picture placement after writing docs chapter ([a545217](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/a545217e309b416bc800aff98698201d4e4198e9))


### Redactional Changes

* footer: change 'BA' to 'Bachelor-Thesis' ([b928fd2](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/b928fd2a3a1847ace277f0f6dc6d2bf7e4c500ac))
* **implementation:** move link to docs deployed url from build process to docs chapter ([0a5d6bf](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/0a5d6bfae462935657ed77ff13193e2b40f1bd5e))
* **implementation/build-process:** add subchapter headings ([f773dd6](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/f773dd68ca66d016705149eabb55481fc477fd7a))
* **sources:** add/update various docs related resource links ([a301b2a](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/a301b2a438920075c4e7d086929833135bba7411))

## [0.5.0](https://gitlab.com/9Lukas5/hft-if7-ba/-/compare/0.4.0...0.5.0) (2020-11-24)


### Style Changes

* **assets/plantuml:** change placement for field/return type to convention ([fc82867](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/fc828673cbe6f38ec54c74bfcc2a369ae44ca4d7))


### Redactional Changes

* **fundamentals/plantuml:** update german 'graphisch' to new spelling ([27a2882](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/27a2882859e084d2dad0b4cb3aac1599bf1c3126))
* **glossary:** add cicd acronym ([4b5036e](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/4b5036e097a327d0a14ac22e2caeedd53c1386c1))
* **glossary:** add cite to fluent-interface entry ([c23e5e4](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/c23e5e4159bed828cdeee020d2bccb464cab0a16))
* **glossary:** add devops entry ([c505bc1](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/c505bc1a675c28c15800b5fdefc3dcdb8e24e22c))
* **sources:** add links for transfer portal, mkdocs, devops and cicd ([1058628](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/105862803772ef8d500b4d369430ebec8a8da40c))
* **sources:** add PlantUML link ([41b9286](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/41b92863fe4c50cfb8c3a3c76be58d7c1391d401))
* **titlepage:** add second validator ([725fcc4](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/725fcc4e7a3ae6961eff8f4fc05a502c4c695523))


### Content Changes

* **assets/plantuml:** add cicd pipelines activity diagrams ([f9b0fc0](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/f9b0fc00bbcc1531c8655703f4e7199c226a7a8e))
* **fundamentals:** add plantuml class diagramm description ([4cfcf2d](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/4cfcf2d52949a7ea8d51af5cfe559be5402b3c3a))
* **implementation:** write build-process chapter ([d42a4a6](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/d42a4a60966a2396c3fe46c4517cd1a61b5d97fe))

## [0.4.0](https://gitlab.com/9Lukas5/hft-if7-ba/-/compare/0.3.0...0.4.0) (2020-11-20)


### Redactional Changes

* **implementation/systems:** replace 'parsbar' with 'konvertierbar' ([a7d5610](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/a7d5610472cee849168b432af774b9de8e7e98d2))
* **implementation/systems:** replace 'Parsbarkeit' with 'Konvertierbarkeit' ([e00d8f1](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/e00d8f19c92411e3578a98b57d5d9b3128b8bdd0))
* **sources:** add JUnit5 and Mockito links ([757beed](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/757beed0e7056d9dbb53a5807388b5229900364e))


### Content Changes

* **implementation:** write unittest chapter ([323fe15](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/323fe150259025a932d2f9f2d5476bf01d51827a)), closes [#2](https://gitlab.com/9Lukas5/hft-if7-ba/-/issues/2)

## [0.3.0](https://gitlab.com/9Lukas5/hft-if7-ba/-/compare/0.2.0...0.3.0) (2020-11-18)


### Style Changes

* **impl/coreclasses/logging:** change graphics placement ([f29ea9d](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/f29ea9dab0c41e746496c30ae7d897335da32919))
* show chapter numbers on pdf bookmarks ([9d70fa3](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/9d70fa3eebea86a192eb6ba209307e1ac4d5db3d))


### Redactional Changes

* **sources:** add OkHttp3 and FasterXML links ([130ce42](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/130ce424c489806ac55125ad3dc26dd89888688b))


### Content Changes

* **implementation:** add chapter for system integration ([2cafb74](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/2cafb7423b41ab9323d92a98dc4e809da027764e))
* **implementation:** write detailed content for gitlab integ ([d356b2b](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/d356b2b612f0c71ab0f16866cfa34178d9e8910c))

## [0.2.0](https://gitlab.com/9Lukas5/hft-if7-ba/-/compare/0.1.0...0.2.0) (2020-11-13)


### Bug Fixes

* **ci:** call plantuml explicitly with utf-8 charset ([89bcbb1](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/89bcbb1c5b878513d295fc3108895264bc86f2c7))


### Style Changes

* add ttfamily to listings basicstyle ([88f2a13](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/88f2a136fbabda6d2b64f91afd525082aeaae58e))
* fix warnings with lstinline inside wrapfig ([325c8ea](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/325c8ead97724cfc25b2e20db895aa08c82763eb))
* set default basicstyle for listings ([0b82535](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/0b82535ad19dcae5c8082e846d1466023e9e9d91))


### Content Changes

* **assets/plantuml:** add Filter class to impl_classes repr ([08d51d4](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/08d51d40fec07521a889f55fa6a4a277b3c9823b))
* **assets/plantuml:** add first view single-class diagrams for chapter core-classes ([4645e0f](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/4645e0f1a31d33f052e4594cd1ea4c732d6380aa))
* **assets/plantuml:** add Logger class to impl_classes repr ([fbd7977](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/fbd7977d235d76080119cac04ddde8f78ba6ffe9))
* **assets/plantuml:** add Ticket class to impl_classes repr ([db1ef11](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/db1ef1142725b36906172e26c20860522c25c709))
* **assets/plantuml:** add TicketAssignee class to impl_classes repr ([00eeb45](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/00eeb4511ea1f911755ab7d2c3b275122ac90794))
* **assets/plantuml:** add uml overview for class calling overview ([b873722](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/b8737228d55cd49c513b1f0a64b0efad0092f36a))
* **fundamentals:** add fluent interface ([827a6a5](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/827a6a5d2d41f8f547cc271fb092fdf857283077))
* **implementation:** add first state of impl. chapter ([a349818](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/a349818338058f34344cdbfa261a80228b020b6a))
* **implementation:** add impl. text for core-class Filter ([8faf1e7](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/8faf1e765b904c5ec950afde0199062f86deb577))
* **implementation:** add impl. text for core-class Logger ([8f3f3d4](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/8f3f3d491ac27b080d70863fae3f33819772a4b0))
* **implementation:** add impl. text for core-class Ticket ([6eb5ea8](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/6eb5ea8754dad5c4fdf2f36f764d90e08027d40a))
* **implementation:** add impl. text for core-class TicketAssignee ([b20cde8](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/b20cde8b08bbfe631ee8a34e7887a6875723b2b1))


### Redactional Changes

* **assets/puml:** fix typo in concept create ticket ([eb59a8b](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/eb59a8bfb5ce2c253863142b2f726ac7f14b7225))
* **concept:** make a few tweaks to the diagram includes ([56219b9](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/56219b9b3b666525a4631003f4eb75b6d625accd))
* **glossary:** add fluent interface entry ([856216a](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/856216a5363be63311d4a89c6f365f91a6f445ef))
* **glossary:** add noop entry ([f5743ca](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/f5743ca4b4a2c1abee15e3ca536f864eafb2129a))
* **implementation:** make various spelling and grammar fixes ([fb1041c](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/fb1041ce802b3e01e658c24628bc05a5fc730155))
* **sources:** add fluent interface wikipedia article to bib ([fc3e5da](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/fc3e5da6ec92265f518ae4e1eec58ca035879e98))

## [0.1.0](https://gitlab.com/9Lukas5/hft-if7-ba/-/compare/0.0.0...0.1.0) (2020-10-14)


### Content Changes

* import current state ([928712b](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/928712be32981fed8fbb1091176ddcbe72db899a))


### Redactional Changes

* add assets ([71e439b](https://gitlab.com/9Lukas5/hft-if7-ba/-/commit/71e439bfa72b1ba7af6a44d4bf69d2f9a1f5812d))
