ensure_path( 'TEXINPUTS', './latex-styles//' );

add_cus_dep('sbl', 'sym', 0, 'makeglossaries');
add_cus_dep( 'acn', 'acr', 0, 'makeglossaries' );
add_cus_dep( 'glo', 'gls', 0, 'makeglossaries' );
$clean_ext .= " acr acn alg glo gls glg sbl sym slg";
sub makeglossaries {
    my ($base_name, $path) = fileparse( $_[0] );
    pushd $path;
    my $return = system "makeglossaries", $base_name;
    popd;
    return $return;
}

$latex = 'latex  %O  --shell-escape %S';
$pdflatex = 'lualatex  %O  --shell-escape %S';
